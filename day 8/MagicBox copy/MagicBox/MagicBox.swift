//
//  MagicBox.swift
//  MagicBox
//
//  Created by Tran Quoc Cuong on 12/26/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import UIKit

class MagicBox: UIViewController {
    
    var statusHeight = UIApplication.sharedApplication().statusBarFrame.height
    var margin: CGFloat = 20
    var k : UIBox!
    var pack = ColorPack()
    let colors = ["#556270", "#‎4ECDC4", "#‎C7F464", "#‎FF6B6B", "#‎C44D58"]
    let colors1 = ["#69D2E7", "#A7DBD8", "#E0E4CC", "#F38630", "#FA6900"]
    let colors2 = ["#594F4F", "#547980", "#45ADA8", "#9DE0AD", "#E5FCC2"]
    let colors3 = ["#FF9900", "#424242", "#E9E9E9", "#BCBCBC", "#3299BB"]
    let colors4 = ["#00A8C6", "#40C0CB", "#F9F2E7", "#AEE239", "#8FBE00"]
    let colors5 = ["#5D4157", "#838689", "#A8CABA", "#CAD7B2", "#EBE3AA"]
    let colors6 = ["#DDE0CF", "#C6BE9A", "#AD8B32", "#937460", "#8C5B7B"]
    let colors7 = ["#595B5A", "#14C3A2", "#0DE5A8", "#7CF49A", "#B8FD99"]
    let colors8 = ["#411F2D", "#AC4147", "#F88863", "#FFC27F", "#FFE29A"]
    let colors9 = ["#CCB24C", "#F7D683", "#FFFDC0", "#FFFFFD", "#457D97"]
    let colors10 = ["#FFF3DB", "#E7E4D5", "#D3C8B4", "#C84648", "#703E3B"]
    let colors11 = ["#D4F7DC", "#DBE7B4", "#DBC092", "#E0846D", "#F51441"]
    let colors12 = ["#951F2B", "#F5F4D7", "#E0DFB1", "#A5A36C", "#535233"]
    let colors13 = ["#9CDDC8", "#BFD8AD", "#DDD9AB", "#F7AF63", "#633D2E"]
    let colors14 = ["#FFFBB7", "#A6F6AF", "#66B6AB", "#5B7C8D", "#4F2958"]
    
    var gesture : UISwipeGestureRecognizer!
    var gestureleft: UISwipeGestureRecognizer!
    
    
    override func loadView() {
        super.loadView()
        
        pack.add(colors)
        pack.add(colors1)
        pack.add(colors2)
        pack.add(colors3)
        pack.add(colors4)
        pack.add(colors5)
        pack.add(colors6)
        pack.add(colors7)
        pack.add(colors8)
        pack.add(colors9)
        pack.add(colors10)
        pack.add(colors11)
        pack.add(colors12)
        pack.add(colors13)
        pack.add(colors14)
        
        view.backgroundColor = UIColor.whiteColor()
        
        var navigatorHeight = navigationController!.navigationBar.frame.height
        var x = margin
        var y = statusHeight + navigatorHeight + 40
        
        var width = view.frame.width - 2 * margin
        
        k = UIBox(width: width, color: colors,location: CGPoint(x: x,y: y))
        
        for z in k.multiBox {
            view.addSubview(z)
        }

        
    }
    
    override func viewDidLoad() {
        gesture = UISwipeGestureRecognizer(target: self, action: "run:")
        gesture.direction = UISwipeGestureRecognizerDirection.Right
        view.addGestureRecognizer(gesture)
        
        gestureleft = UISwipeGestureRecognizer(target: self, action: "run2:")
        gestureleft.direction = UISwipeGestureRecognizerDirection.Left
        view.addGestureRecognizer(gestureleft)
        
    }
    
    func run(sender: UIGestureRecognizer) {
//        if gesture.direction == UISwipeGestureRecognizerDirection.Left {
//            var chooseColor = pack.next()
//            println(pack.startPoint)
//            changeColor(k, chooseColor)
//        } else {
            var chooseColor = pack.pre()
            println(pack.startPoint)
            changeColor(k, chooseColor,1)
 //       }

        
    }
    
    func run2(sender: UIGestureRecognizer) {
        var chooseColor = pack.next()
        println(pack.startPoint)
        changeColor(k, chooseColor, -1)
    }
 
    func changeColor(box : UIBox, _ color:[String],_ a: CGFloat) {
        UIView.animateWithDuration(1, animations: {
        
//            box.multiBox[1].transform = CGAffineTransformMakeRotation(CGFloat(M_PI * 3 / 4))
//            for var i = 2; i < countElements(box.multiBox); i++ {
//                box.multiBox[i].transform = CGAffineTransformMakeRotation(CGFloat(M_PI_4 * Double(i)))
//                box.multiBox[1].backgroundColor = UIColor(hex: color[i], alpha: 1)
//            }
//            box.multiBox[0].backgroundColor = UIColor(hex: color[0], alpha: 1)
//            box.multiBox[1].backgroundColor = UIColor(hex: color[1], alpha: 1)
            box.multiBox[0].backgroundColor = UIColor(hex: color[0], alpha: 1)
            for var i = 1; i < countElements(box.multiBox); i++ {
                //get current angle
                let radians = atan2( box.multiBox[i].transform.b, box.multiBox[i].transform.a)
                var newrad = (radians + CGFloat(M_PI_2) * a )
//                if a < 0 && newrad > 0 {
//                    newrad  = newrad * a
//                }
//                if a > 0 && newrad < 0 {
//                    newrad = newrad * -a
//                }
                box.multiBox[i].transform = CGAffineTransformMakeRotation(CGFloat(newrad))
                box.multiBox[i].backgroundColor = UIColor(hex: color[i], alpha: 1)
//                box.multiBox[i].transform = CGAffineTransformIdentity
            }
            
            }, completion: {
                finished in
            })
    
    }
    

}