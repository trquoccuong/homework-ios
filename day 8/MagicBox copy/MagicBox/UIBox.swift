//
//  UIBox.swift
//  MagicBox
//
//  Created by Tran Quoc Cuong on 12/28/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import UIKit

class UIBox: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    var multiBox = [UIView]()
    init(width: CGFloat, color: [String], location: CGPoint) {
        super.init(frame: CGRectMake(location.x, location.y, width, width))
        multiBox.append(self)
        var newsize = width
        self.backgroundColor = UIColor(hex: color[0], alpha: 1)
        for i  in 1...4 {
            newsize = newsize / sqrt(2)
            var subBox = UIView(frame: CGRectMake(0, 0, newsize, newsize))
            subBox.center = self.center
            subBox.backgroundColor = UIColor(hex: color[i], alpha: 1)
            subBox.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_4 * Double(i)))
            multiBox.append(subBox)
        }
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
