//
//  ColorPack.swift
//  MagicBox
//
//  Created by Tran Quoc Cuong on 12/28/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//



class ColorPack {
    var array = [[String]]()
    var startPoint = 0
    
    func add(z:[String]) {
        array.append(z)
    }
    func next() -> [String] {
        startPoint++
        if startPoint < countElements(self.array) {
            return array[startPoint]
        } else {
            startPoint = 0
            return array[startPoint]
        }
    }
    
    func pre() -> [String] {
        startPoint--
        if startPoint >= 0 {
            return array[startPoint]
        } else {
            startPoint = countElements(self.array) - 1
            return array[startPoint]
        }
    }
}