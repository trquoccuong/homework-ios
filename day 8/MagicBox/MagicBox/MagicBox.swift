//
//  MagicBox.swift
//  MagicBox
//
//  Created by Tran Quoc Cuong on 12/26/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import UIKit

class MagicBox: UIViewController {
    var rect : UIView!
    var rectWidth: CGFloat = 0
    let margin: CGFloat = 20
    var statusHeight: CGFloat = 0
    var navigatorHeight: CGFloat = 0
    var colors = ["#556270", "#‎4ECDC4", "#‎C7F464", "#‎FF6B6B", "#‎C44D58"]
    // var colors = ["#69D2E7", "#A7DBD8", "#E0E4CC", "#F38630", "#FA6900"]
    
    var gesture : UISwipeGestureRecognizer!
    override func loadView() {
        super.loadView()
        view.backgroundColor = UIColor.whiteColor()
        
    }
    override func viewDidLoad() {
        gesture = UISwipeGestureRecognizer(target: self, action: "run:")
        view.addGestureRecognizer(gesture)
        
    }
    func run(sender: UIGestureRecognizer) {
        if gesture.direction == UISwipeGestureRecognizerDirection.Right {
            reOderColor()
            creatBox(-1)
        }
    }
    func reOderColor(){
        var c = colors.removeLast()
        colors.insert(c, atIndex: 0)
    }
    
    
    
    func creatBox(k: CGFloat) {
        
        statusHeight = UIApplication.sharedApplication().statusBarFrame.height
        navigatorHeight = navigationController!.navigationBar.frame.height
        var x = margin
        var y = statusHeight + navigatorHeight + 40
        var recwid =  (view.frame.width - 2 * margin) / 2
        var z = CGFloat(1)
        for i in 0...1 {
            for j in 0...1 {
                var frame = CGRect(x: recwid * CGFloat(j) + margin, y: y + recwid * CGFloat(i), width: recwid, height: recwid)
                var box = UIView(frame: frame)
                if j % 2 == 0 {
                    z =  i % 2 == 0 ?   k : -k
                } else {
                    z =  i % 2 == 0 ?  -k : k
                }
                rect = UIView(frame: box.frame)
                
                rect.center = box.center
                
                rect.backgroundColor = UIColor(hex: colors[0], alpha: 1)
                view.addSubview(rect)
                
                var k =  rect.frame.width
                for i in 1...4 {
                    var angle = M_PI_4 * Double(i) * Double(z)
                    var miniWidth = k / sqrt(2)
                    k = miniWidth
                    
                    var miniRect = UIView(frame: CGRect(x: 0, y: 0, width: miniWidth, height: miniWidth))
                    miniRect.center = rect.center
                    miniRect.backgroundColor = UIColor(hex: colors[i], alpha: 1)
                    view.addSubview(miniRect)
                    // var option = UIViewAnimationOptions.Autoreverse | UIViewAnimationOptions.Repeat
                    var option = nil
                    UIView.animateWithDuration( 1, delay: 0, options: option, animations: {
                        miniRect.transform = CGAffineTransformMakeRotation(CGFloat(angle * 3))
                        miniRect.backgroundColor = UIColor(hex: self.colors[i], alpha: 1)
                        self.rect.backgroundColor = UIColor(hex: self.colors[0], alpha: 1)
                        },
                        completion: {
                            finished in
                    }
                )
                }
                
            }
        }

    }

}
