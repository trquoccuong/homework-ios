// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//
var dictionary:[Int:String] = [ 1 : "one",2 : "two", 3:"three", 4:"four", 5: "five", 6: "six", 7: "seven", 8: "eight", 9: "nine", 10: "ten", 11: "eleven", 12: "twelve",13: "thirteen", 14: "fourteen", 15: "fifteen", 16: "sixteen", 17: "seventeen", 18: "eighteen", 19: "nineteen", 20: "twenty", 30: "thirty", 40: "forty", 50: "fifty", 60: "sixty", 70: "seventy", 80:"eighty", 90:"ninety" , 100:"hundred" ,1000: "thousand", 1000000:"million", 1000000000:"billion",1000000000000: "trillion" ]


//
func numbertoArray(number : Int,heso: Int) -> [Int]{
    var myNumber = number
    var myArray = [Int]()
    while myNumber > 0 {
        myArray.append(myNumber%heso)
        myNumber /= heso
    }
    return myArray
    
}

// DOc so duoi 100
func number100ToString(number: Int) ->String {
    var result = ""
    if number < 100 {
        if let stringNumber = dictionary[number] {
            result += stringNumber
        } else {
            if let stringNumber = dictionary[number / 10 * 10] {
                result += stringNumber + " "
            }
            if let stringNumber = dictionary[ number % 10 ] {
                result += stringNumber
            }
        }
    }
    return result
    
}

// Doc so duoi 1000
func number1000toString(number: Int) ->String {
    var result = ""
    if number < 100 {
        result += number100ToString(number)
    } else if number < 1000 {
        var hundredNumber  = number / 100
        if let stringNumber = dictionary[hundredNumber] {
            result += stringNumber + " "
        }
        if let stringNumber = dictionary[100] {
            result +=  stringNumber + " "
        }
        result += number100ToString(number%100)
    }
    return result
}

// Doc so

func numberToString(number: Int) -> String {
    
    if let stringNumber  = dictionary[number] {
        return stringNumber
    }
    else {
        var result = ""
        var arrayNumber = numbertoArray(number, 1000)
        while arrayNumber != [] {
            var decimalIndex = countElements(arrayNumber)
            //tinh gia tri 10^x
            var truenumber = 1
            for var i = 1; i < decimalIndex; i++ {
                truenumber  *= 1000
            }
            // tinh he so
            var heso = arrayNumber.removeLast()
            
           result += number1000toString(heso) + " "
            
            if countElements(arrayNumber) != 0 {
                if let truenumberString = dictionary[truenumber] {
                    result += truenumberString + " " + ","
                }
            }
            
        }
        
        
        return result
    }
}






