// Playground - noun: a place where people can play

import UIKit


var normalArray = ["a","an","the","i", "he","she","they","we","are","is","be","them","our","am","to","there"]

func splitAndFilter(a: String) ->[String] {
    var arrayWord = a.componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString: " ,./?[]123456789()"))
    var changeArray = arrayWord.filter({!contains(normalArray, $0.lowercaseString)})
    return changeArray
}

func frequencyWord(a: [String]) ->[String:Int] {
    var frequencyWord = [String:Int]()
    for word in a {
        if let newFreq = frequencyWord[word] {
            frequencyWord[word] = newFreq + 1
        } else {
            frequencyWord[word] = 1
        }
    }
    return frequencyWord
}

var k = frequencyWord(splitAndFilter("aaaa aa aa aa ff fffff aa"))

//sort freqency
func sortByFreq(dic:[String:Int]) -> [String] {
    var myDic = dic
    var myArray = Array(myDic.keys).sorted(<)
    return myArray
}


