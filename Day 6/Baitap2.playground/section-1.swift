// Playground - noun: a place where people can play

import UIKit

class Car {
    var name :String
    var manufacturer: String
    var price: Int
    init (_ name:String, _ manufacturer:String, _ price: Int) {
        self.name = name
        self.manufacturer = manufacturer
        self.price = price
    }
}

var listCar = [Car]()

listCar.append(Car("Morning","KIA", 1000))
listCar.append(Car("City","Honda", 1000))
listCar.append(Car("Civic","Honda", 1000))
listCar.append(Car("Accord","Honda", 1000))
listCar.append(Car("CR-V","Honda", 1000))
listCar.append(Car("F36","BMW", 1000))
listCar.append(Car("F45","BMW", 1000))
listCar.append(Car("F46","BMW", 1000))
listCar.append(Car("A1","Audi", 1000))
listCar.append(Car("A8","Audi", 1000))
listCar.append(Car("Q3","Audi", 1000))
listCar.append(Car("Altima","Nissan", 1000))
listCar.append(Car("Rogue","Nissan", 1000))
listCar.append(Car("IKon","Ford", 1000))
listCar.append(Car("Equus","Huyndai", 1000))

func changePrice(inout listCar: [Car] ,type: String, price: Double) -> String {
    var newString = ""
    for car in listCar {
        if car.manufacturer == type {
            car.price += Int(Double(car.price) * price)
            newString += car.name + " \(car.price) \n"
        }
    }
    return newString
}
println(changePrice(&listCar, "Honda", 0.2))

println(changePrice(&listCar, "BMW", -0.2))







