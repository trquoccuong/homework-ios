//
//  CountWord.swift
//  Baitap1
//
//  Created by Tran Quoc Cuong on 12/17/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import UIKit
let normalArray = ["a","an","the","i", "he","she","they","we","are","is","be","them","our","am","to","there"]



class CountWord: ConsoleScreen {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.main()
    }
    
    func main() {
        if let content = getContent() {
            var makeArray = splitString(content)
            var newArray = filterString(makeArray)
            var freqDic = frequencyWord(newArray)
            var finalArray =  sortByFreq(freqDic)
            for word in finalArray {
                self.writeln(word)
            }
            
        }
    }
    

    // get content from TXT
    func getContent() -> String?{
        if  let pathTXT = NSBundle.mainBundle().pathForResource("data", ofType: "txt") {
            if let content = String(contentsOfFile: pathTXT, encoding: NSUTF8StringEncoding, error: nil) {
                return content
            }
        }
        return nil
    }
    //get normal word
    func getWord() -> [String] {
        var arrayWord = [String]()
        if  let pathTXT = NSBundle.mainBundle().pathForResource("normalWord", ofType: "txt") {
            if let content = String(contentsOfFile: pathTXT, encoding: NSUTF8StringEncoding, error: nil) {
                    arrayWord = splitString(content)
            }
        }
        return arrayWord
    }
    //get Characterset
    func getCharacterset() -> NSCharacterSet{
        var array = NSMutableCharacterSet()
        if  let pathTXT = NSBundle.mainBundle().pathForResource("character", ofType: "txt") {
            if let content = String(contentsOfFile: pathTXT, encoding: NSUTF8StringEncoding, error: nil) {
                array = NSMutableCharacterSet(charactersInString: content) as NSMutableCharacterSet
            }
        }
        //add character new Line
        array.addCharactersInString("/n")
        return array
    }
 
    // split String 
    func splitString(a: String) -> [String] {
        var arrayWord = [String]()
        var characterSet = getCharacterset()
        arrayWord = a.componentsSeparatedByCharactersInSet(characterSet)
        return arrayWord
    }
    // Filter String
    
    func filterString(a: [String]) -> [String] {
        var filterString = [String]()
        var normalWord = getWord()
        
        filterString = a.filter({!contains(normalWord, $0.lowercaseString)})
        
        return filterString
    }
    
    /* combine split and filter
    func splitAndFilter(a: String) ->[String]? {
        var arrayWord = a.componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString: "\n '*,./?[]123456789()!"))
        var changeArray = arrayWord.filter({!contains(normalArray, $0.lowercaseString)})
        return changeArray
    }*/
    
    func frequencyWord(a: [String]) ->[String:Int] {
        var frequencyWord = [String:Int]()
        for word in a {
            if let newFreq = frequencyWord[word] {
                frequencyWord[word] = newFreq + 1
            } else {
                frequencyWord[word] = 1
            }
        }
        return frequencyWord
    }
    
    //sort freqency
    func sortByFreq(dic:[String:Int]) -> [String] {
        var myDic = dic
        var myArray = Array(myDic.keys).sorted(<)
        return myArray
    }

}
