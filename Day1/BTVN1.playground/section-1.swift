// Playground - noun: a place where people can play

import UIKit


//Giai phuong trinh bac nhat ax + b = 0
func giaiPTbac1(a: Double,b: Double) -> String {
    if a == 0 {
        if b == 0 {
            return "Phuong trinh vo so nghiem"
        }
        else {
            return "Phuong trinh vo nghiem"
        }
    }
    else {
        if b == 0 {
            return "Phuon trinh co 1 nghiem x = 0"
        }
        else {
            return "Phuong trinh co 1 nghiem x = \(-b/a)"
        }
    }
    
}
giaiPTbac1(4, 5)

//Giai phuong trinh bac 2 ax2 +bx +c = 0

func giaiPTBac2(a: Double,b: Double,c: Double) -> String {
    if a == 0{
        return giaiPTbac1(b,c)
    }
    else {
        var delta  =  (b*b) - 4*a*c
        if delta > 0 {
            return "Phuong trinh co 2 nghiem phan biet x1 = \((-b - sqrt(delta))/(2*a)) va x2 = \((-b + sqrt(delta))/(2*a))"
        }
        else if delta == 0 {
            return "Phuong trinh co nghiem kep x1 = x2 = \(-b/(2*a))"
        }
        else {
            return "Phuong trinh vo nghiem"
        }
    }
}

giaiPTBac2(1, 2, 1)
//Tinh giai thua bang while

func giaiThua(n: UInt) -> UInt{
    var ketqua: UInt = 1
    var i : UInt = 0
    if n == 0 {
        return ketqua
            }
    else{
        while  i < n {
            i++
            ketqua *= i
        }
        return ketqua
    }
}

//Tinh tong cac so tu nhien tu 1 den N bang vong lap for

func tinhTong(n:UInt) -> UInt {
    var ketqua: UInt = 0
    for i in 1...n {
        ketqua += i
    }
    return ketqua
}

tinhTong(5)
//class Person

class Person {
    var firstName: String?
    var lastName: String?
    var score: Float?
}

let person = Person()

person.firstName = "Cuong"
person.lastName = "Tran"
person.score = 5.0

