// Playground - noun: a place where people can play

import UIKit
// 6.1 
func getPentagonalNumber(n: Int) -> Int {
    return n*(n*3 - 1)/2
}
for var i = 1; i <= 100 ; i++ {
    println(getPentagonalNumber(i))
}

// 6.2
func splitNumber(n: Int) -> [Int] {
    var numbers = [Int]()
    var dividend = n
    while dividend > 0 {
        numbers.append(dividend%10)
        dividend = dividend / 10
    }
    return numbers
}
func sumDigits(n: Int) ->Int {
    var numbers = splitNumber(n)
    var sum = 0
    for number in numbers {
        sum += number
    }
    return sum
}

sumDigits(36)
// 6.3
func reverse(n: Int ) -> Int {
    var numbers = splitNumber(n)
    var reverseString = ""
    for number in numbers {
        reverseString += "\(number)"
    }
    var reverseNumber: Int? = reverseString.toInt()
    return reverseNumber!
}

println(reverse(1234))
func isPalindrome(n: Int) -> Bool {
    return n == reverse(n)
}

isPalindrome(121)
// 6.4 -> 6.3 reverse
// 6.5
func displaySortedNumber(numbers: Int...) ->[Int]{
    var numbersSorted = [Int]()
    var numbersUnSort = numbers
    while numbersUnSort.count != 0 {
        var minNumber = numbers[0]
        var indexNumber = 0
        var minIndex = 0
        for number in numbersUnSort{
            if number < minNumber {
                minNumber = number
                minIndex = indexNumber
            }
            indexNumber += 1
        }
        numbersSorted.append(minNumber)
        numbersUnSort.removeAtIndex(minIndex)
    }
    return numbersSorted
}

//6.6
func displayPattern(n: Int) {
    for var i = 1; i <= n ; i++ {
        for var j = 1; j <= i; j++ {
            print("\(j) ")
        }
        println()
    }
}

displayPattern(5)
//6.7

func futureInvestmentValue(amount: Double, rate: Double, year: Int) -> Double {
    var futureValue = amount
    println("Year      Amount")
    for var i = 1; i <= year ; i++ {
        futureValue += (futureValue * rate)
        println("\(i)         \(futureValue)")
    }
    return futureValue
}

futureInvestmentValue(10000, 0.05, 10)

//6.8

func celciusToFahrenheit(celsius: Double) -> Double {
    return (9 / 5 ) * celsius + 32
}

func fahrenheitToCelsius(fahrenheit: Double) -> Double {
    return (5 / 9) * (fahrenheit - 32 )
}










