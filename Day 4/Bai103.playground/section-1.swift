// Playground - noun: a place where people can play

import UIKit

class MyInteger {
    var value: Int
    //Class method with Int
    
    class func isEven(number: Int)-> Bool {
        return number%2 == 0
    }
    
    class func isOdd(number: Int)-> Bool {
        return number%2 != 0
    }
    
    class func isPrime(number: Int) -> Bool {
        var arrayDividable = [Int]()
        for var i = 1; i <= number;i++ {
            if number % i == 0 {
                arrayDividable.append(i)
            }
        }
        return arrayDividable.count == 2
    }
    class func parseInt(numArray: [String]) -> Int {
        var string: String = "".join(numArray)
        return string.toInt()!
    }
    
    class func parseInt(string: String) -> Int{
        return string.toInt()!
    }
    // Class method with MyInterger
    class func isEven(number: MyInteger)-> Bool {
        return number.getValue()%2 == 0
    }
    
    class func isOdd(number: MyInteger)-> Bool {
        return number.getValue()%2 != 0
    }
    
    class func isPrime(number: MyInteger) -> Bool {
        var arrayDividable = [Int]()
        for var i = 1; i <= number.getValue();i++ {
            if number.getValue() % i == 0 {
                arrayDividable.append(i)
            }
        }
        return arrayDividable.count == 2
    }
    // Constructor
    init(value: Int) {
        self.value = value
    }
    // Type method
    func getValue()->Int{
        return self.value
    }
    
    func isEven()-> Bool {
        return self.value%2 == 0
    }
    
    func isOdd()-> Bool {
        return self.value%2 != 0
    }
    
    func isPrime() -> Bool {
        var arrayDividable = [Int]()
        for var i = 1; i <= self.value;i++ {
            if self.value % i == 0 {
                arrayDividable.append(i)
            }
        }
        return arrayDividable.count == 2
    }
    
    func equals( number: Int) -> Bool {
        return self.getValue() == number
    }
    
    func equals(number: MyInteger) -> Bool {
        return self.getValue() == number.getValue()
    }
    
}





// Test class

var number1 =  MyInteger(value: 4)
number1.isEven()