// Playground - noun: a place where people can play

import UIKit

class Account {
    var balance: Double
    var ID: String!
    var name: String
    
    init(ID: String, balance: Double, name: String) {
        self.balance = balance
        self.ID = ID
        self.name = name
        
    }
    
    func checkBalance() ->Double {
        return balance
    }
    
    func withdraw(money: Double) ->Bool {
        if money < balance {
            self.balance -= money
            return true
        }
        else {
            return false
        }
        
    }
    
    func deposit(money: Double) ->Bool {
        if money >= 0 {
            self.balance += money
            return  true
        }
        else {
            return false
        }
    }
   
}
