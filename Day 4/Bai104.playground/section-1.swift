// Playground - noun: a place where people can play

import UIKit

class MyPoint {
    var  x:Double
    var y:Double
    init() {
        self.x = 0
        self.y = 0
    }
    init(x:Double,y:Double) {
        self.x = x
        self.y = y
    }
    func distance(point: MyPoint) -> Double {
        return sqrt(pow((self.x - point.x),2) + pow((self.y - point.y),2) )
    }
    func distance(x: Double, y:Double) -> Double {
        return sqrt(pow((self.x - x),2) + pow((self.y - y),2) )
    }
    
}

var pointA = MyPoint()
var pointB = MyPoint(x: 5, y: 4)
pointB.distance(pointA)
