// Playground - noun: a place where people can play

import UIKit

class StackOfIntegers {
    var stack:[Int]
    init() {
        self.stack = [Int]()
    }
    func push(number: Int) {
        stack.append(number)
    }
    
    func pop() ->Int{
        return stack.removeLast()
    }
    func length() -> Int{
        return stack.count
    }
    
    class func reverse(newStack: StackOfIntegers) ->StackOfIntegers{
        var reverseStack = StackOfIntegers()
        for var i = 0 ; i < newStack.length() ; i++ {
            reverseStack.push(newStack.pop())
        }
        return reverseStack
    }
}

func primeFactors(number: Int) -> StackOfIntegers{
    var dividend = number
    var newStack = StackOfIntegers()
    for var i = 2; i <= dividend; i++
    {
        while dividend % i == 0 {
            newStack.push(i)
            dividend = dividend / i
        }
    }
    return newStack
}

primeFactors(120)
// long test Prime
func isPrime(number: Int) -> Bool {
    var newStack = StackOfIntegers()
    for var i = 1; i <= number; i++ {
        if number % i == 0 {
            newStack.push(i)
        }
    }
    return newStack.length() == 2
}
// fast test Prime
func isPrime2(number: Int) ->Bool {
    if number <= 3 {
        return number > 1
    }
    if (number % 2 == 0 ) || (number % 3 == 0) {
        return false
    } else {
        for var i = 5; i*i <= number ; i += 6  {
            if ( number % i == 0) || ( number % (i + 2) == 0){
                return false
            }
        }
        return true
    }
}

isPrime(10)

func displayPrime(number: Int) -> StackOfIntegers{
    var newStack = StackOfIntegers()
    for var i = 1; i <= number; i++ {
        if isPrime2(i) {
            newStack.push(i)
        }
    }
    return newStack
}
// Qua ton thoi gian
func displayPrime2(number: Int) -> StackOfIntegers{
    var newStack = StackOfIntegers()
    var primeArray = [Int]()
    for var i = 1; i <= number; i++ {
        //neu gia tri i chia het cho 1 gia tri trong stack thi  out
        var checkPrime = true
        if checkPrime {
            for c in primeArray {
                if i % c == 0 {
                    checkPrime  = false
                }
            }
            if checkPrime && isPrime2(i) {
                newStack.push(i)
                primeArray.append(i)
            }
        }
        
    }
    return newStack
}

//displayPrime2(2000)
displayPrime(2000)

