//
//  ViewController.swift
//  baitap4
//
//  Created by Tran Quoc Cuong on 12/3/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var speedOutlet: UITextField!
    @IBOutlet weak var accOutlet: UITextField!
    @IBOutlet weak var resultOutlet: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func calculate(sender: UIButton) {
        var speed = (speedOutlet.text as NSString).doubleValue
        var acc = (accOutlet.text as NSString).doubleValue
        
        resultOutlet.text = "\((speed * speed)/( 2 * acc))"
 
    }


}

