//
//  ViewController.swift
//  baitap2
//
//  Created by Tran Quoc Cuong on 12/3/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var inTempOutlet: UITextField!
    @IBOutlet weak var amountOutlet: UITextField!
    @IBOutlet weak var fiTempOutlet: UITextField!
    @IBOutlet weak var resultOutlet: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func calculate(sender: UIButton) {
        
            var inTemp = (inTempOutlet.text as NSString).doubleValue
            var finalTemp = (fiTempOutlet.text as NSString).doubleValue
            var amount = (amountOutlet.text as NSString).doubleValue
            var result = 0.0
            
            result = amount * (finalTemp - inTemp) * 4184
            resultOutlet.text = "\(result)"

    }




}

