//
//  ViewController.swift
//  baitap3
//
//  Created by Tran Quoc Cuong on 12/3/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var numberOutlet: UITextField!
    @IBOutlet weak var populationOutlet: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func calculate(sender: UIButton) {
        var year = numberOutlet.text.toInt()!
        var population = 0
        var second = year * 365*24*60*60
        
        population = 312032486 + second/7 - second/13 + second/45
        
        populationOutlet.text = "\(population)"
    }



}

