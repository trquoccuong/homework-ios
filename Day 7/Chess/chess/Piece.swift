//
//  Piece.swift
//  Chess
//
//  Created by Tran Quoc Cuong on 12/20/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import UIKit

class Piece {
    var canMove = false
    var imageView:UIImageView
    var canMoveArray = [Cell]()
    var cel: Cell?
    
    init(name: String) {
        self.imageView = UIImageView(image: UIImage(named: name))
        self.imageView.transform = CGAffineTransformMakeScale(0.03, 0.03)
    }
    
    func center() -> CGPoint {
        return self.imageView.center
    }
    func setCenter(point: CGPoint) {
        self.imageView.center = point
    }
    func setMoveArray(array: [Cell]){
     //   clearMoveArray()
        self.canMoveArray = array
    }
    
    func clearMoveArray() {
      while canMoveArray != [] {
        canMoveArray.removeLast()
       }
    }
    
    func isMove(point: CGPoint) -> Bool {
        var result = false
        for cell in canMoveArray {
            if cell.view.frame.contains(point) {
                result = true
            }
        }
        return result
    }
    
    func removeFriendCell(friend:Piece) {
    }

    


}
