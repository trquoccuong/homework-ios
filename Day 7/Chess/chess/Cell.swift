//
//  Cell.swift
//  Chess
//
//  Created by Tran Quoc Cuong on 12/20/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import UIKit


class Cell: NSObject {
    var row: Int
    var col: Int
    var view: UIView
    
    init(row: Int, col:Int, view: UIView){
        self.row = row
        self.col = col
        self.view = view
    }
    subscript(row: Int,column: Int) -> String {
        return "\(row) \(column)"
    }
    func call() -> String {
        return "\(self.row) \(self.col)"
    }
}
