//
//  Knight.swift
//  Chess
//
//  Created by Tran Quoc Cuong on 12/21/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import Foundation
import UIKit

class Knight: Piece {
    init() {
        super.init(name: "Anonymous_Chess_symbols_set_11")
    }
    
    override func setMoveArray(arrayCell: [Cell]){
        self.clearMoveArray()
        if let cellPiece = self.cel {
            var currentRow = cellPiece.row
            var currentCol = cellPiece.col
            for cell in arrayCell {
                if abs(currentCol - cell.col) == 2 && abs(currentRow - cell.row) == 1 {
                    self.canMoveArray.append(cell)
                }
                if abs(currentCol - cell.col) == 1 && abs(currentRow - cell.row) == 2 {
                    self.canMoveArray.append(cell)
                }
            }
        }
        
    }
    
}



