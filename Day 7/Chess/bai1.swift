//
//  bai1.swift
//  Chess
//
//  Created by Tran Quoc Cuong on 12/21/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import UIKit

class bai1: UIViewController {

    var arrayCell = [Cell]()
    var arrayCellCanMove = [Cell]()
    var margin: CGFloat = 10
    var marginTop: CGFloat = 160
    var knight: Knight!
    var arrayPieceFriend = [Piece]()
    var timer:NSTimer!
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = UIColor.lightGrayColor()
        let cellWidth = (view.bounds.size.width - margin * 2.0 ) / 8.0
        for rowIndex in 0...7 {
            for colIndex in 0...7 {
                let rect = CGRect(x: margin + cellWidth * CGFloat(colIndex), y: cellWidth * CGFloat(rowIndex) + marginTop, width: cellWidth, height: cellWidth)
                let cellView = UIView(frame: rect)
                if colIndex % 2 == 0 {
                    cellView.backgroundColor = rowIndex % 2 == 0 ? UIColor.grayColor() : UIColor.whiteColor()
                } else {
                    cellView.backgroundColor = rowIndex % 2 == 0 ? UIColor.whiteColor() : UIColor.grayColor()
                }
                arrayCell.append(Cell(row: rowIndex, col: colIndex, view: cellView))
                view.addSubview(cellView)
            }
        }
        
        var textLabel = UIButton(frame: CGRect(x: view.center.x, y: 200 + cellWidth * 8, width:200, height: 50))
        textLabel.setTitle("Automatic", forState: nil)
        textLabel.backgroundColor = UIColor.whiteColor()
        textLabel.setTitleColor(UIColor.blackColor(), forState: nil)
        textLabel.addTarget(self, action: "setAuto:", forControlEvents: .TouchUpInside)
        textLabel.center.x = view.center.x
        view.addSubview(textLabel)
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "update", userInfo: nil, repeats: false)
        

        
        //Add Knight
        knight = Knight()
        var e = randomCell()
        knight.setCenter(e.view.center)
        knight.cel = e
        knight.canMove = true
        arrayPieceFriend.append(knight)
        view.addSubview(knight.imageView)
        
        

        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    func update() {
        autoMove()
    }
    
    func setAuto(sender:UIButton!) {
        if timer .valid {
            timer.invalidate()
        } else {
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "update", userInfo: nil, repeats: true)
        }
        
    }
    
    
    
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        for touch in touches {
            var point  = touch.locationInView(view)
            if let cells = checkTouch(point) {
                // Kiem tra xem tren ban co co con the move chua
                if let chessMove = checkCanMove() {
                    if let chessChoosed = checkPieceInCell(cells) {
                        
                        printRedCell(chessChoosed)
                        // Con co do trung voi con co duoc chon thi
                        if chessChoosed.center() == chessMove.center(){
                        } else {
                            chessMove.canMove = false
                            chessChoosed.canMove =  true
                            removeRed()
                        }
                    } else {
                        // neu o click co trong array can move thi move
                        if chessMove.isMove(point) {
                            chessMove.setCenter(cells.view.center)
                            chessMove.cel = cells
                            
                            removeRed()
                        }
                        
                    }
                    
                }
            }
            
        }
    }
    
    func autoMove() {
        for piece in arrayPieceFriend {
            makeArrayCellCanMove(piece)
            var randomNumber = Int(arc4random_uniform(UInt32(countElements(piece.canMoveArray))))
            println(randomNumber)
            piece.setCenter(piece.canMoveArray[randomNumber].view.center)
            piece.cel = piece.canMoveArray[randomNumber]
        }
    }
    
    
    
    
    // Ham kiem tra o nao chua touch
    func checkTouch(g: CGPoint) -> Cell? {
        var cellCheck:Cell?
        for cell in arrayCell {
            if cell.view.frame.contains(g) {
                cellCheck = cell
            }
        }
        return cellCheck
    }
    // check xem o do da co con co nao chua neu co tra ve gia tri con co
    func checkPieceInCell(cell:Cell) -> Piece? {
        var result : Piece?
        for piece in arrayPieceFriend {
            if cell.view.frame.contains(piece.center()) {
                result = piece
            }
        }
        return result
    }
    //Ham check xem co con co nao duoc chon hay ko
    
    func checkCanMove() -> Piece? {
        for piece in arrayPieceFriend {
            if piece.canMove == true {
                makeArrayCellCanMove(piece)
                return piece
            } else {
            }
        }
        return nil
    }
    
    //In ra man hinh
    
    func printRedCell(piece:Piece) {
        makeArrayCellCanMove(piece)
        for cell in piece.canMoveArray {
            view.addSubview(cell.view)
        }
    }
    
    
    // loai bo cac o do
    func removeRed() {
        while arrayCellCanMove != [] {
            arrayCellCanMove.removeLast().view.removeFromSuperview()
        }
    }

    
    //Ham tao cac o di chuyen duoc
    func makeArrayCellCanMove(piece: Piece){
        // loai bo vi tri cac con co dang co tren ban co
        for cell in arrayCell {
            var newView = UIView(frame: cell.view.frame)
            var minicell = Cell(row: cell.row, col: cell.col, view: newView)
            minicell.view.backgroundColor = UIColor.redColor()
            minicell.view.alpha = 0.2
            if let k =  checkPieceInCell(cell){
            } else {
                arrayCellCanMove.append(minicell)
            }
        }
        piece.setMoveArray(arrayCellCanMove)
        
    }

    
    
    
    // make a randomCell in array Cell
    func randomCell() -> Cell{
        var number: UInt32 = UInt32(countElements(arrayCell))
        var randomCell =  arc4random_uniform(number)
        return arrayCell[Int(randomCell)]
        
    }


}
